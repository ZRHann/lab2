**操作系统原理 实验二（RUST）**

## 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336316

【姓名】郑锐涵

## 实验题目

操作系统的启动

## 实验目的

1. 了解页表的作用、ELF 文件格式、操作系统在 x86 架构的基本启动过程。
2. 尝试使用 UEFI 加载并跳转到内核执行内核代码。
3. 实现基于 uart16550 的串口驱动，使用宏启用输出能力、并启用日志系统。
4. 学习并尝试使用调试器对内核进行调试。

## 实验要求

1. 编译内核 ELF
2. 在 UEFI 中加载内核
3. UART 与日志输出
4. 思考题
5. 加分项

## 实验方案

RUST路线。

系统环境：Debian 12实体机（Intel x64）。

项目开发环境：gcc, make, python, qemu，rust

编辑器：vscode

终端：windows terminal

## 实验过程

### 编译内核 ELF

编译到了`0x01/target/x86_64-unknown-none/release`下。

![image-20240306172345937](report2.assets/image-20240306172345937.png)

![image-20240306172413027](report2.assets/image-20240306172413027.png)

![image-20240306173743997](report2.assets/image-20240306173743997.png)

![image-20240306173813175](report2.assets/image-20240306173813175.png)

```ld
ENTRY(_start)
KERNEL_BEGIN = 0xffffff0000000000;
SECTIONS {
  . = KERNEL_BEGIN;
  .rodata ALIGN(4K):
  {
    *(.rodata .rodata.*)
  }
  .text ALIGN(4K):
  {
    *(.text .text.*)
  }
  .data ALIGN(4K):
  {
    *(.data .data.*)
  }
  .got ALIGN(4K):
  {
    *(.got .got.*)
  }
  .bss ALIGN(4K):
  {
    *(.bss .bss.*)
  }
}
```

**1. 请查看编译产物的架构相关信息，与配置文件中的描述是否一致？**

1. **架构信息（Architecture）**：

   配置文件中的 `arch` 字段为 `"x86_64"`，`readelf` 输出结果中的 "Machine" 为 `Advanced Micro Devices X86-64`，一致。

2. **端序（Endianness）**：

   配置文件`"target-endian": "little"`，`readelf` 的 "Data" 字段为 `2's complement, little endian`，都是小端序。

3. **指针宽度**：

   配置文件中的 `target-pointer-width` 字段为 `"64"`，表明目标指针宽度为 64 位。`readelf` 输出结果中的 "Class" 字段为 `ELF64`，一致。

   

**2. 找出内核的入口点，它是被如何控制的？结合源码、链接、加载的过程，谈谈你的理解。**

入口点 `0xffffff0000000000`。

1.  `.cargo/config.toml`，指定了

```toml
[build]
target = "config/x86_64-unknown-none.json"
```

`config/x86_64-unknown-none.json`文件定义了一个自定义目标规范。这告诉Rust编译器如何编译代码以适应特定的目标环境。



2. `config/x86_64-unknown-none.json`，指定了

   ```json
   "pre-link-args": {
       "ld.lld": ["-Tpkg/kernel/config/kernel.ld", "-export-dynamic"]
     }
   ```

   `"pre-link-args"`：定义了链接前的参数，`"-Tpkg/kernel/config/kernel.ld"`指定了使用自定义的链接脚本`kernel.ld`。

3.  `config/kernel.ld`，指定了

   ```
   KERNEL_BEGIN = 0xffffff0000000000;
   ```

从而确定了内核的入口点。



**3.请找出编译产物的 segments 的数量，并且用表格的形式说明每一个 segments 的权限、是否对齐等信息。**

```bash
readelf -l ysos_kernel
```

| Segment | 类型         | 权限 | 对齐   | 虚拟地址           | 物理地址           | 文件大小 | 内存大小 |
| ------- | ------------ | ---- | ------ | ------------------ | ------------------ | -------- | -------- |
| 1       | LOAD         | R    | 0x1000 | 0xffffff0000000000 | 0xffffff0000000000 | 0x393f4  | 0x393f4  |
| 2       | LOAD         | R E  | 0x1000 | 0xffffff000003a000 | 0xffffff000003a000 | 0x3a2a5  | 0x3a2a5  |
| 3       | LOAD         | RW   | 0x1000 | 0xffffff0000075000 | 0xffffff0000075000 | 0x6148   | 0x6148   |
| 4       | LOAD         | RW   | 0x1000 | 0xffffff000007c000 | 0xffffff000007c000 | 0x0      | 0x20     |
| 5       | DYNAMIC      | RW   | 0x8    | 0xffffff000007b0c8 | 0xffffff000007b0c8 | 0x80     | 0x80     |
| 6       | GNU_RELRO    | R    | 0x1    | 0xffffff000007b000 | 0xffffff000007b000 | 0x148    | 0x148    |
| 7       | GNU_EH_FRAME | R    | 0x4    | 0xffffff00000393c8 | 0xffffff00000393c8 | 0xc      | 0xc      |
| 8       | GNU_STACK    | RW   | 0x0    | 0x0                | 0x0                | 0x0      | 0x0      |



### 在 UEFI 中加载内核

#### 1. Load config

```rust
 let config = config::Config { 
     kernel_stack_address: 0xFFFF_FF01_0000_0000,
     kernel_stack_auto_grow: 0,
     kernel_stack_size: 512,
     physical_memory_offset: 0xFFFF_8000_0000_0000,
     kernel_path: "\\KERNEL.ELF",
     cmdline: "",
     load_apps: false, 
};
```

复制了`DEFAULT_CONFIG`.

#### 2.Load ELF files

```rust
let mut file = open_file(bs, config.kernel_path);
let buf = load_file(bs, &mut file);
let elf = match ElfFile::new(buf) {
    Ok(elf) => elf,
    Err(err) => {
        error!("Failed to parse ELF file: {:?}", err);
        return Status::ABORTED;
    }
};
```

#### 3.更新控制寄存器

```rust
// FIXME: root page table is readonly, disable write protect (Cr0)
    unsafe {
        Cr0::update(|flags| {
            flags.remove(Cr0Flags::WRITE_PROTECT);
        });
    }
```

#### 4.映射内核文件

```rust
// FIXME: map physical memory to specific virtual address offset
let mut frame_allocator = UEFIFrameAllocator(bs);
elf::map_physical_memory(config.physical_memory_offset, max_phys_addr, &mut page_table, &mut frame_allocator);
// FIXME: map physical memory to specific virtual address offset
let mut frame_allocator = UEFIFrameAllocator(bs);
elf::map_physical_memory(config.physical_memory_offset, max_phys_addr, &mut page_table, &mut frame_allocator);

// FIXME: load and map the kernel elf file
for segment in elf.program_iter() {
    elf::load_segment(buf.as_ptr(), config.physical_memory_offset, &segment, &mut page_table, &mut frame_allocator).unwrap();
}
```

代码中有

```rust
let stacktop = config.kernel_stack_address + config.kernel_stack_size * 0x1000 - 8;
```

这里stacktop应该是初始栈顶，即栈底。`kernel_stack_address`应该是最高的栈顶。

所以map的时候，从kernel_stack_address开始map即可。

```rust
// FIXME: map kernel stack
elf::map_range(config.kernel_stack_address, config.kernel_stack_size * 4096, &mut page_table, &mut frame_allocator);

// FIXME: recover write protect (Cr0)
unsafe {
    Cr0::update(|flags| {
        flags.insert(Cr0Flags::WRITE_PROTECT);
    });
}
```

#### 5.补全 `load_segment`函数

```rust
	// FIXME: handle page table flags with segment flags
    let flags = segment.flags();
    if flags.is_execute() {
        page_table_flags.remove(PageTableFlags::NO_EXECUTE);
    }
    else {
        page_table_flags.insert(PageTableFlags::NO_EXECUTE);
    }

    if flags.is_write() {
        page_table_flags.insert(PageTableFlags::WRITABLE);
    }
    else {
        page_table_flags.remove(PageTableFlags::WRITABLE);
    }
```

#### 6.使用vscode调试内核

##### 报错1

```rust
// FIXME: load and map the kernel elf file
    for segment in elf.program_iter() {
        elf::load_segment(buf.as_ptr(), config.physical_memory_offset, &segment, &mut page_table, &mut frame_allocator).unwrap();
    }
```

```
called `Result::unwrap()` on an `Err` value: PageAlreadyMapped(PhysFrame[4KiB](0x4280000))
```

发现vscode似乎没法进入文件调试（已经配置好），于是先设置

```rust
log::set_max_level(log::LevelFilter::Trace);
```

定位到

```
[TRACE]: pkg/elf/src/lib.rs@158: Mapping page: 0xffffff000007b000
[TRACE]: pkg/elf/src/lib.rs@184: Zeroing rest of the page: 0xffffff000007b000
[TRACE]: pkg/elf/src/lib.rs@122: Loading & mapping segment: Ph64(
[TRACE]: pkg/elf/src/lib.rs@122:     ProgramHeader64 {
[TRACE]: pkg/elf/src/lib.rs@122:         type_: Ok(
[TRACE]: pkg/elf/src/lib.rs@122:             Load,
[TRACE]: pkg/elf/src/lib.rs@122:         ),
[TRACE]: pkg/elf/src/lib.rs@122:         flags: Flags(
[TRACE]: pkg/elf/src/lib.rs@122:             0x6,
[TRACE]: pkg/elf/src/lib.rs@122:         ),
[TRACE]: pkg/elf/src/lib.rs@122:         offset: 0x7d000,
[TRACE]: pkg/elf/src/lib.rs@122:         virtual_addr: 0xffffff000007c000,
[TRACE]: pkg/elf/src/lib.rs@122:         physical_addr: 0xffffff000007c000,
[TRACE]: pkg/elf/src/lib.rs@122:         file_size: 0x0,
[TRACE]: pkg/elf/src/lib.rs@122:         mem_size: 0x20,
[TRACE]: pkg/elf/src/lib.rs@122:         align: 0x1000,
[TRACE]: pkg/elf/src/lib.rs@122:     },
[TRACE]: pkg/elf/src/lib.rs@122: )
[TRACE]: pkg/elf/src/lib.rs@149: Segment page table flag: PageTableFlags(PRESENT | WRITABLE | NO_EXECUTE)
[TRACE]: pkg/elf/src/lib.rs@122: Loading & mapping segment: Ph64(
[TRACE]: pkg/elf/src/lib.rs@122:     ProgramHeader64 {
[TRACE]: pkg/elf/src/lib.rs@122:         type_: Ok(
[TRACE]: pkg/elf/src/lib.rs@122:             Dynamic,
[TRACE]: pkg/elf/src/lib.rs@122:         ),
[TRACE]: pkg/elf/src/lib.rs@122:         flags: Flags(
[TRACE]: pkg/elf/src/lib.rs@122:             0x6,
[TRACE]: pkg/elf/src/lib.rs@122:         ),
[TRACE]: pkg/elf/src/lib.rs@122:         offset: 0x7c0c8,
[TRACE]: pkg/elf/src/lib.rs@122:         virtual_addr: 0xffffff000007b0c8,
[TRACE]: pkg/elf/src/lib.rs@122:         physical_addr: 0xffffff000007b0c8,
[TRACE]: pkg/elf/src/lib.rs@122:         file_size: 0x80,
[TRACE]: pkg/elf/src/lib.rs@122:         mem_size: 0x80,
[TRACE]: pkg/elf/src/lib.rs@122:         align: 0x8,
[TRACE]: pkg/elf/src/lib.rs@122:     },
[TRACE]: pkg/elf/src/lib.rs@122: )
[TRACE]: pkg/elf/src/lib.rs@149: Segment page table flag: PageTableFlags(PRESENT | WRITABLE | NO_EXECUTE)
[TRACE]: pkg/elf/src/lib.rs@158: Mapping page: 0xffffff000007b000
[PANIC]: panicked at pkg/boot/src/main.rs:96:121:
```

倒数第二个segment的file_size是0，所以跳过了map。

而最后一个segment，地址0xffffff000007b0c8在0xffffff000007b000内，已经被map，可能因此导致了错误。

注意到，所有segment里面，只有最后一个的type是Dynamic，其他的都是Load。

查找了一些资料，只需要加载Load类型的段即可。修改代码如下：

```rust
// FIXME: load and map the kernel elf file
    info!("Loading and mapping the kernel ELF file...");
    for segment in elf.program_iter() {
        if segment.get_type().unwrap() != xmas_elf::program::Type::Load {
            continue;
        }
        elf::load_segment(buf.as_ptr(), config.physical_memory_offset, &segment, &mut page_table, &mut frame_allocator).unwrap();
    }
```

##### 报错2

```
[ INFO]: pkg/boot/src/main.rs@103: Mapping kernel stack...
[TRACE]: pkg/elf/src/lib.rs@050: Page Range: PageRange { start: Page[4KiB](0xffffff0100000000), end: Page[4KiB](0xffffff0300000000) }(2097152)
[PANIC]: panicked at pkg/boot/src/allocator.rs:12:14:
Failed to allocate frame: Error { status: OUT_OF_RESOURCES, data: () }
```

计算了以下分配的页面数，刚好是512*4096，应该是对kernel_stack_size的单位理解错了。

于是修改：（去掉了*4096）

```
info!("Mapping kernel stack...");
    elf::map_range(config.kernel_stack_address, config.kernel_stack_size, &mut page_table, &mut frame_allocator);
```

##### 成功进入kernal

解决了上述报错后成功进入了kernal。

![image-20240315172555236](report2.assets/image-20240315172555236.png)

查看断点处的汇编和符号：

![image-20240315175440735](report2.assets/image-20240315175440735.png)

内核的加载情况在编译内核ELF时已经使用`readelf`查看。

#### 7.回答问题

**`set_entry` 函数做了什么？为什么它是 unsafe 的？**

`set_entry`函数的作用是设置一个全局变量`ENTRY`的值，这个值代表了要跳转执行的入口点地址，给 `jump_to_entry`使用。



**`jump_to_entry` 函数做了什么？要传递给内核的参数位于哪里？查询 `call` 指令的行为和 x86_64 架构的调用约定，借助调试器进行说明。**

将`stacktop`的值移动到`rsp`寄存器中，即设置新的栈顶地址。

call跳转到ENTRY的地址。

将boot_info的地址存入rdi寄存器中。

![image-20240315210929223](report2.assets/image-20240315210929223.png)

调试结果显示，此时rdi确实是boot_info的地址。

要传递给内核的参数即boot_info，位于rdi寄存器中。



**`entry_point!` 宏做了什么？内核为什么需要使用它声明自己的入口点？**

定义一个名为 `_start` 的函数，操作系统或启动器（bootloader）加载内核时，会查找并调用这个函数。



**如何为内核提供直接访问物理内存的能力？你知道几种方式？代码中所采用的是哪一种？**

1. Identity Mapping：虚拟地址与物理地址一一对应。会占用大量的虚拟地址空间。
2. Mapping at a Fixed Offset：在虚拟地址空间中预留一块区域专门用于页表映射，从而避免了身份映射的缺点。
3. Mapping the Complete Physical Memory：将整个物理内存映射到虚拟地址空间的一个固定偏移处。内核可以随时访问任意物理内存，包括其他地址空间的页表帧。
4. Temporary Mapping：只在需要时临时映射页表帧。
5. Recursive Page Tables：通过将四级页表的某个入口映射到四级页表本身，实现了对所有当前和未来页表帧的映射。



**为什么 ELF 文件中不描述栈的相关内容？栈是如何被初始化的？它可以被任意放置吗？**

因为栈是在程序运行时由操作系统动态分配和管理的，不需要在文件格式中静态描述。栈的初始化通常由操作系统在程序加载到内存执行时完成，栈的位置可以有一定的灵活性，但通常会放在进程地址空间的高地址处。



**请解释指令 `layout asm` 的功能。倘若想找到当前运行内核所对应的 Rust 源码，应该使用什么 GDB 指令？**

将GDB的界面布局切换到汇编视图。这种布局显示当前函数的汇编代码，以及一个箭头指示当前执行的指令。

`list`列出当前执行点的上下文源代码。



**假如在编译时没有启用 `DBG_INFO=true`，调试过程会有什么不同？**

缺少符号信息，无法看到清晰的函数调用栈、局部变量的名称和值，无法准确地定位到源代码中的行。



**你如何选择了你的调试环境？截图说明你在调试界面（TUI 或 GUI）上可以获取到哪些信息？**

Vscode。截图在上面。



### UART 与日志输出

#### 1.串口驱动的实现

```rust
use core::fmt;
use core::arch::asm;

/// A port-mapped UART 16550 serial interface.
pub struct SerialPort {
    port: u16, // 串行端口的基地址，用于访问串行端口的各个寄存器。
}


/// 写入一个字节到指定的端口。
///
/// # 参数
///
/// * `port` - 端口号。
/// * `value` - 要写入的字节。
unsafe fn outb(port: u16, value: u8) {
    asm!("out dx, al", in("dx") port, in("al") value, options(nomem, nostack));
}


/// 从指定的端口读取一个字节。
///
/// # 参数
///
/// * `port` - 端口号。
///
/// # 返回
///
/// 返回从端口读取的字节。
unsafe fn inb(port: u16) -> u8 {
    let value: u8;
    asm!("in al, dx", out("al") value, in("dx") port, options(nomem, nostack));
    value
}

impl SerialPort {
    pub const fn new(port: u16) -> Self {
        Self { port }
    }

    /// 初始化串行端口。
    pub fn init(&self) {
        unsafe {
            outb(self.port + 1, 0x00); // 禁用所有中断。
            outb(self.port + 3, 0x80); // 启用DLAB（Divisor Latch Access Bit），允许设置波特率除数。
            outb(self.port + 0, 0x03); // 设置波特率除数的低字节为3（38400波特率）。
            outb(self.port + 1, 0x00); // 设置波特率除数的高字节。
            outb(self.port + 3, 0x03); // 设定数据格式为8位字符，无奇偶校验位，1个停止位。
            outb(self.port + 2, 0xC7); // 启用FIFO（First In First Out），清除缓冲区，并设置14字节的阈值。
            outb(self.port + 4, 0x0B); // IRQs使能，准备RTS（Request to Send）和DSR（Data Set Ready）信号。
        }
    }

    /// 发送一个字节到串行端口。
    pub fn send(&mut self, data: u8) {
        unsafe {
            while inb(self.port + 5) & 0x20 == 0 {} // 等待直到传输缓冲区为空。(port+5)是LSR寄存器，第5位是THRE位，表示传输缓冲区为空。
            outb(self.port, data); // 将数据写入端口，发送出去。
        }
    }

    /// 无等待地从串行端口接收一个字节。
    pub fn receive(&mut self) -> Option<u8> {
        unsafe {
            if inb(self.port + 5) & 0x01 != 0 { // 检查是否有数据可读。
                Some(inb(self.port)) // 如果有，从端口读取数据。
            } else {
                None // 如果没有数据可读，返回None。
            }
        }
    }
}

impl fmt::Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            self.send(byte);
        }
        Ok(())
    }
}
```

#### 2.串口驱动的测试

![image-20240324164735991](report2.assets/image-20240324164735991.png)

如图，成功初始化。

#### 3.日志输出

```rust
	fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("[{}] {}", record.level(), record.args());
        }
    }
```



### 思考题

**在 `pkg/kernel` 的 `Cargo.toml` 中，指定了依赖中 `boot` 包为 `default-features = false`，这是为了避免什么问题？请结合 `pkg/boot` 的 `Cargo.toml` 谈谈你的理解。**

boot里的features：

```
[features]
boot = ["uefi/alloc", "uefi-services"]
default = ["boot"]
```

不自动启用`["uefi/alloc", "uefi-services"]`。避免使用std。



**在 `pkg/boot/src/main.rs` 中参考相关代码，聊聊 `max_phys_addr` 是如何计算的，为什么要这么做？**

```rust
	let mmap = system_table
        .boot_services()
        .memory_map(mmap_storage)
        .expect("Failed to get memory map");

    let max_phys_addr = mmap
        .entries()
        .map(|m| m.phys_start + m.page_count * 0x1000)
        .max()
        .unwrap()
        .max(0x1_0000_0000); // include IOAPIC MMIO area
```

`mmap`：系统的内存映射。

`.entries()`：内存映射的每个条目（内存区域）。

`.map(|m| m.phys_start + m.page_count * 0x1000)`：对每个内存区域，计算其最大（结束）物理地址。然后组成一个新的集合。

起始地址 + 页数 * 0x1000（每个页面的大小） = 该区域的结束物理地址。

`.max().unwrap()`：所有内存区域的物理地址的最大值。

`.max(0x1_0000_0000)`：确保计算出的最大物理地址至少为`0x1_0000_0000`。目的是确保计算出的地址包含一个重要区域。（I/O高级可编程中断控制器（IOAPIC）的内存映射输入输出（MMIO）区域，和处理中断有关）



**串口驱动是在进入内核后启用的，那么在进入内核之前，显示的内容是如何输出的？**

使用UEFI提供的SystemTable。即 `uefi_services::init(&mut system_table)`。



**假如我们将 `Makefile` 中取消该选项，QEMU 的输出窗口会发生什么变化？请观察指令 `make run QEMU_OUTPUT=` 的输出，结合截图分析对应现象。**

![image-20240324220547451](report2.assets/image-20240324220547451.png)

![image-20240324220640310](report2.assets/image-20240324220640310.png)

进入kernal后的输出并没有显示到显示器，也没有显示到标准输出。



**在移除 `-nographic` 的情况下，如何依然将串口重定向到主机的标准输入输出？请尝试自行构造命令行参数，并查阅 QEMU 的文档，进行实验。**

```
-serial stdio
```

将串口连接到标准输入输出。

即

```
make run QEMU_OUTPUT="-serial stdio"
```

![image-20240324221814888](report2.assets/image-20240324221814888.png)



### 加分项

**😋 线控寄存器的每一比特都有特定的含义，尝试使用 `bitflags` 宏来定义这些标志位，并在 `uart16550` 驱动中使用它们。**

```rust
use core::fmt;
use core::arch::asm;
use bitflags::bitflags;

/// A port-mapped UART 16550 serial interface.
pub struct SerialPort {
    port: u16, // 串行端口的基地址，用于访问串行端口的各个寄存器。
}

// port+1 IER
bitflags! {
    pub struct IER_Bits: u8 {
        const RECEIVED_DATA_AVAILABLE = 0x01; // 接收数据可用中断
        const TRANSMITTER_HOLDING_REGISTER_EMPTY = 0x02; // 发送保持寄存器空中断
        const RECEIVER_LINE_STATUS = 0x04; // 接收线状态中断
        const MODEM_STATUS = 0x08; // 调制解调器状态中断
    }
}

// port+2 IIR   
bitflags! {
    pub struct IIR_Bits: u8 {
        const ENABLE_FIFO = 0x01; // 启用FIFO
        const CLEAR_RECEIVE_FIFO = 0x02; // 清除接收FIFO
        const CLEAR_TRANSMIT_FIFO = 0x04; // 清除发送FIFO
        const DMA_MODE_SELECT = 0x08; // DMA模式选择
        // 16550A模型特有
        const ENABLE_64_BYTE_FIFO = 0x20; // 启用64字节FIFO（如果硬件支持）
    }
}

// port+3 LCR
bitflags! {
    pub struct LLR_Bits: u8 {
        const DATA_BITS_5 = 0x00; // 5位数据位
        const DATA_BITS_6 = 0x01; // 6位数据位
        const DATA_BITS_7 = 0x02; // 7位数据位
        const DATA_BITS_8 = 0x03; // 8位数据位
        const STOP_BITS_1 = 0x00; // 1停止位
        const STOP_BITS_2 = 0x04; // 2停止位（如果设置为5位数据位，则为1.5停止位）
        const PARITY_NONE = 0x00; // 无奇偶校验
        const PARITY_ODD = 0x08; // 奇校验
        const PARITY_EVEN = 0x18; // 偶校验
        const PARITY_MARK = 0x28; // 标记校验（始终为1）
        const PARITY_SPACE = 0x38; // 空白校验（始终为0）
        const SET_BREAK = 0x40; // 设置中断使能
        const DLAB = 0x80; // 分频锁存访问位（DLAB）
    }
}

// port+4 MCR
bitflags! {
    pub struct MCR_Bits: u8 {
        const DTR = 0x01; // 数据终端就绪
        const RTS = 0x02; // 请求发送
        const OUT1 = 0x04; // 辅助输出1
        const OUT2 = 0x08; // 辅助输出2，用于中断使能
        const LOOP = 0x10; // 环回测试模式
    }
}

// port+5 LSR
bitflags! {
    pub struct LSR_Bits: u8 {
        const DATA_READY = 0x01; // 数据准备就绪
        const OVERRUN_ERROR = 0x02; // 接收器溢出错误
        const PARITY_ERROR = 0x04; // 奇偶校验错误
        const FRAMING_ERROR = 0x08; // 帧错误
        const BREAK_INTERRUPT = 0x10; // 中断信号检测
        const TRANSMITTER_EMPTY = 0x20; // 发送保持寄存器为空
        const TRANSMITTER_HOLDING_REGISTER_EMPTY = 0x40; // 发送器空，即所有数据已发送
        const IMPENDING_ERROR = 0x80; // 在FIFO模式下，表示接收FIFO中至少有一个帧错误或溢出错误
    }
}

// 以下是UART 16550的寄存器偏移定义（假设基础端口为`port`）
const RBR: u16 = 0; // 接收缓冲寄存器(Receiver Buffer Register), 只读, 偏移0，用于读取接收到的数据
const THR: u16 = 0; // 发送保持寄存器(Transmitter Holding Register), 只写, 偏移0，用于写入发送的数据
const DLL: u16 = 0; // 分频锁存低字节(Divisor Latch Low Byte), 可读写, 偏移0，设置波特率时使用
const IER: u16 = 1; // 中断使能寄存器(Interrupt Enable Register), 可读写, 偏移1
const DLH: u16 = 1; // 分频锁存高字节(Divisor Latch High Byte), 可读写, 偏移1，设置波特率时使用
const IIR: u16 = 2; // 中断识别寄存器(Interrupt Identification Register), 只读, 偏移2，用于识别中断源
const FCR: u16 = 2; // FIFO控制寄存器(FIFO Control Register), 只写, 偏移2，用于配置FIFO
const LCR: u16 = 3; // 线控寄存器(Line Control Register), 可读写, 偏移3，用于配置数据格式
const MCR: u16 = 4; // 调制解调器控制寄存器(Modem Control Register), 可读写, 偏移4，用于控制调制解调器
const LSR: u16 = 5; // 线状态寄存器(Line Status Register), 只读, 偏移5，用于读取发送和接收状态
const MSR: u16 = 6; // 调制解调器状态寄存器(Modem Status Register), 只读, 偏移6，用于读取调制解调器状态
const SCR: u16 = 7; // 刮擦寄存器(Scratch Register), 可读写, 偏移7，备用

/// 写入一个字节到指定的端口。
///
/// # 参数
///
/// * `port` - 端口号。
/// * `value` - 要写入的字节。
unsafe fn outb(port: u16, value: u8) {
    asm!("out dx, al", in("dx") port, in("al") value, options(nomem, nostack));
}


/// 从指定的端口读取一个字节。
///
/// # 参数
///
/// * `port` - 端口号。
///
/// # 返回
///
/// 返回从端口读取的字节。
unsafe fn inb(port: u16) -> u8 {
    let value: u8;
    asm!("in al, dx", out("al") value, in("dx") port, options(nomem, nostack));
    value
}

impl SerialPort {
    pub const fn new(port: u16) -> Self {
        Self { port }
    }

    unsafe fn write_port(&self, offset: u16, value: u8) {
        unsafe {
            outb(self.port + offset, value);
        }
    }

    unsafe fn read_port(&self, offset: u16) -> u8 {
        unsafe {
            inb(self.port + offset)
        }
    }

    /// 初始化串行端口。
    pub fn init(&self) {
        unsafe {
            self.write_port(IER, IER_Bits::empty().bits()); // 禁用所有中断
            self.write_port(LCR, LLR_Bits::DLAB.bits()); // 启用DLAB
            self.write_port(DLL, 0x03); // 设置波特率低字节
            self.write_port(DLH, 0x00); // 设置波特率高字节
            self.write_port(LCR, LLR_Bits::DATA_BITS_8.bits()); // 8位数据位，无奇偶校验，1停止位
            self.write_port(FCR, IIR_Bits::ENABLE_FIFO.bits() | IIR_Bits::CLEAR_RECEIVE_FIFO.bits() | IIR_Bits::CLEAR_TRANSMIT_FIFO.bits()); // 启用并清除FIFO
            self.write_port(MCR, MCR_Bits::DTR.bits() | MCR_Bits::RTS.bits() | MCR_Bits::OUT2.bits()); // 设置MCR
        }
    }

    /// 发送一个字节到串行端口。
    pub fn send(&mut self, data: u8) {
        unsafe {
            while self.read_port(LSR) & LSR_Bits::TRANSMITTER_EMPTY.bits() == 0 {} // 等待直到传输缓冲区为空。(port+5)是LSR寄存器，第5位是THRE位，表示传输缓冲区为空。
            self.write_port(THR, data) // 将数据写入端口，发送出去。
        }
    }

    /// 无等待地从串行端口接收一个字节。
    pub fn receive(&mut self) -> Option<u8> {
        unsafe {
            if self.read_port(LSR) & LSR_Bits::DATA_READY.bits() != 0 { // 检查是否有数据可读。
                Some(self.read_port(RBR)) // 如果有，从端口读取数据。
            } else {
                None // 如果没有数据可读，返回None。
            }
        }
    }
}

impl fmt::Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            self.send(byte);
        }
        Ok(())
    }
}

```

![image-20240324230748003](report2.assets/image-20240324230748003.png)



**😋 尝试在进入内核并初始化串口驱动后，使用 escape sequence 来清屏，并编辑 `get_ascii_header()` 中的字符串常量，输出你的学号信息。**

```rust
pub const fn get_ascii_header() -> &'static str {
    concat!(
        r"
  ___   ___   ____  ____     __  ____  __    __  
 |__ \ |__ \ |___ \|___ \   / / |___ \/_ |  / /  
    ) |   ) |  __) | __) | / /_   __) || | / /_  
   / /   / /  |__ < |__ < | '_ \ |__ < | || '_ \ 
  / /_  / /_  ___) |___) || (_) |___) || || (_) |
 |____||____||____/|____/  \___/|____/ |_| \___/ 
                                       v",
        env!("CARGO_PKG_VERSION")
    )
}
```

```rust
pub fn kernel_main(boot_info: &'static boot::BootInfo) -> ! {
    ysos::init(boot_info);
    print!("\x1B[2J\x1B[1;1H");
    println!("{}", ysos::get_ascii_header());
    loop {
        info!("Hello World from YatSenOS v2!");
        for _ in 0..0x10000000 {
            unsafe {
                asm!("nop");
            }
        }
    }
}
```

![image-20240324232953129](report2.assets/image-20240324232953129.png)



**🤔 尝试添加字符串型启动配置变量 `log_level`，并修改 `logger` 的初始化函数，使得内核能够根据启动参数进行日志输出。**

对boot中的config.rs：

```rust
pub struct Config<'a> {
    /// The address at which the kernel stack is placed
    pub kernel_stack_address: u64,
    /// The size we need to alloc the init kernel stack, 0 means alloc all
    pub kernel_stack_auto_grow: u64,
    /// The size of the kernel stack, given in number of 4KiB pages
    pub kernel_stack_size: u64,
    /// The offset into the virtual address space where the physical memory is mapped
    pub physical_memory_offset: u64,
    /// The path of kernel ELF
    pub kernel_path: &'a str,
    /// Kernel command line
    pub cmdline: &'a str,
    /// Load apps into memory, when no fs implemented in kernel
    pub load_apps: bool,
    pub log_level: &'a str,
}

pub const DEFAULT_CONFIG: Config = Config {
    kernel_stack_address: 0xFFFF_FF01_0000_0000,
    kernel_stack_auto_grow: 0,
    kernel_stack_size: 512,
    physical_memory_offset: 0xFFFF_8000_0000_0000,
    kernel_path: "\\KERNEL.ELF",
    cmdline: "",
    load_apps: false, 
    log_level: "trace", 
};
```

`process`里的`match key`加上

```rust
"log_level" => self.log_level = value,
```



对kernal里的lib.rs：

```rust
pub fn init(_boot_info: &'static BootInfo) {
    drivers::serial::init(); // init serial output
    logger::init(config::DEFAULT_CONFIG.log_level); // init logger system

    info!("YatSenOS initialized.");
}
```



对logger.rs：

```rust
pub fn init(log_level: &str) {
    static LOGGER: Logger = Logger;
    log::set_logger(&LOGGER).unwrap();
    let level_filter = match log_level {
        _ if log_level.eq_ignore_ascii_case("off") => LevelFilter::Off,
        _ if log_level.eq_ignore_ascii_case("error") => LevelFilter::Error,
        _ if log_level.eq_ignore_ascii_case("warn") => LevelFilter::Warn,
        _ if log_level.eq_ignore_ascii_case("info") => LevelFilter::Info,
        _ if log_level.eq_ignore_ascii_case("debug") => LevelFilter::Debug,
        _ if log_level.eq_ignore_ascii_case("trace") => LevelFilter::Trace,
        _ => LevelFilter::Info, // Default level or handle unknown level as you see fit
    };
    log::set_max_level(level_filter);
    info!("Logger Initialized.");
}
```

经测试，可正常使用。

![image-20240325004441418](report2.assets/image-20240325004441418.png)



**🤔 尝试使用调试器，在内核初始化之后（**`ysos::init` **调用结束后）下断点，查看、记录并解释如下的信息：**

- **内核的栈指针、栈帧指针、指令指针等寄存器的值。**
- **内核的代码段、数据段、BSS 段等在内存中的位置。**

![image-20240325014901116](report2.assets/image-20240325014901116.png)

栈初始地址是0xFFFF_FF01_0000_0000。rsp较其偏移了0x001fff58，表明栈上有这么多空间已经被使用。

而rbp的值是低地址，可能是因为此时刚进入内核，还没有任何函数调用，rbp并没有被设置过，其值取决于bootloader的行为。



![image-20240325020412911](report2.assets/image-20240325020412911.png)

代码段（.text）：[0xffffff0000082000-0xffffff0000082020)

数据段（.data）：[0xffffff000007a000-0xffffff0000080288)

BSS段（.bss）：[0xffffff0000082000-0xffffff0000082020)

这与kernal.ld中的定义一致。



**🤔 “开发者是愿意用安全换取灵活的”，所以，我要把代码加载到栈上去，可当我妄图在栈上执行代码的时候，却得到了** `Segment fault`**，你能解决这个问题吗？**

**请尝试利用 `gcc` 在 Linux 平台上编译一个简单的 C 语言程序，将其编译为 ELF 格式的文件，并尝试在栈上执行它，使它输出 `Hello, world!`。**

编写64位汇编程序

```asm
section .data
    helloMessage db 'helloworld', 0xA  ; 'helloworld' 字符串和一个换行符
    helloLen equ $ - helloMessage      ; 字符串长度

section .text
    global _start

_start:
    ; 写消息到标准输出
    mov rax, 1              ; 'write' 系统调用的编号对于64位是1
    mov rdi, 1              ; 文件描述符 1 表示标准输出
    mov rsi, helloMessage   ; 消息的地址
    mov rdx, helloLen       ; 消息长度
    syscall                 ; 调用内核

    ; 退出程序
    mov rax, 60             ; 'exit' 系统调用的编号对于64位是60
    xor rdi, rdi            ; 退出码 0
    syscall                 ; 调用内核

```

```
nasm -f elf64 ./helloworld.asm -o helloworld.o
ld -m elf_x86_64 -s -o helloworld helloworld.o
objdump -d ./helloworld
```

得到

```
0000000000401000 <.text>:
  401000:       b8 01 00 00 00          mov    $0x1,%eax
  401005:       bf 01 00 00 00          mov    $0x1,%edi
  40100a:       48 be 00 20 40 00 00    movabs $0x402000,%rsi
  401011:       00 00 00 
  401014:       ba 0b 00 00 00          mov    $0xb,%edx
  401019:       0f 05                   syscall
  40101b:       b8 3c 00 00 00          mov    $0x3c,%eax
  401020:       48 31 ff                xor    %rdi,%rdi
  401023:       0f 05                   syscall
```

在栈上执行它（还需要调试，以将0x402000修改为正确的helloworld字符串地址）：

```c
#include <stdio.h>
#include <string.h>
int main() {
    unsigned char shellcode[] = 
    "\xb8\x01\x00\x00\x00"          // mov    $0x1,%eax
    "\xbf\x01\x00\x00\x00"          // mov    $0x1,%edi
    "\x48\xbe\x00\x20\x40\x00\x00"  // movabs $0x402000,%rsi
    "\x00\x00\x00"
    "\xba\x0b\x00\x00\x00"          // mov    $0xb,%edx
    "\x0f\x05"                      // syscall
    "\xb8\x3c\x00\x00\x00"          // mov    $0x3c,%eax
    "\x48\x31\xff"                  // xor    %rdi,%rdi
    "\x0f\x05"                     // syscall
    "helloworld\n";
    void (*func)();
    func = (void (*)()) shellcode;
    (void)(*func)();
    return 0;
}
```

临时禁用ALSR（栈地址随机）：

```
sysctl -w kernel.randomize_va_space=0
```

编译c程序（关闭各种保护，产生调试信息）

```
gcc -z execstack -fno-stack-protector -g -o helloworld helloworld.c
```

gef调试

![image-20240325041202747](report2.assets/image-20240325041202747.png)

可以看到，"Hello World"的地址是0x00007fffffffde05。（H的ascii是0x48）

多次调试，`x 0x00007fffffffde05`都能得到"Hello World"的ascii。

于是修改代码

```
"\x48\xbe\x00\x20\x40\x00\x00"  // movabs $0x402000,%rsi
    "\x00\x00\x00"
```

为

```
"\x48\xbe\x05\xde\xff\xff"  // movabs $0x7fffffffde00,%rsi
"\xff\x7f\x00\x00"
```

这样就完全在栈上执行了打印helloworld的代码。

![image-20240325082436224](report2.assets/image-20240325082436224.png)

只有在gef中运行才会输出helloworld，在bash直接运行无输出，可能这两种执行方式，栈地址会不同。

## 实验总结

在ELF文件格式中，segments（由程序头描述）和sections（由节头描述）是两种不同的实体。Segments主要用于程序的加载，定义了如何将程序的不同部分（比如代码、数据）加载到内存中。Sections则用于存储程序的详细信息，比如代码、数据、符号表等。

- **Segments**（程序头`Program Headers`描述）: 主要面向执行时的内存布局，没有名字，只有类型（比如`LOAD`、`DYNAMIC`等）。
- **Sections**（节头`Section Headers`描述）: 包含详细的程序信息，每个section都有一个名字（比如`.text`、`.data`等）。

在处理ELF（Executable and Linkable Format）文件时，主要关注的是`PT_LOAD`类型的段，这些被标记为可加载的段含有必须加载到内存中以便执行的代码或数据。

内核直接访问物理内存的能力对于操作系统的运行至关重要。它允许内核读写物理内存中的数据，从而控制硬件和执行其它关键任务。

Identity Mapping是最简单的映射方式，虚拟地址与物理地址一一对应。这种方法的主要优点是简单易懂，且在进行内核初始化时较为方便。但它的缺点也很明显，会占用大量的虚拟地址空间，导致在寻找连续的大内存区域时遇到困难，这种问题类似于内存碎片化。

Mapping at a Fixed Offset通过在虚拟地址空间中预留一块区域专门用于页表映射，从而避免了身份映射的缺点。例如，可以选择在虚拟地址空间的10 TiB处开始映射所有页表帧。这种方法避免了身份映射导致的虚拟地址空间碎片化问题，但需要更多的管理工作，并且不允许访问其他地址空间的页表。

Mapping the Complete Physical Memory将整个物理内存映射到虚拟地址空间的一个固定偏移处。这样做的好处是内核可以随时访问任意物理内存，包括其他地址空间的页表帧。但这种方法需要额外的页表来存储物理内存的映射，消耗了一部分物理内存。

Temporary Mapping只在需要时临时映射页表帧。这种方法仅需要一个身份映射的一级页表，可以节省物理内存，但每次访问都需要建立和撤销映射，管理起来较为复杂。

Recursive Page Tables通过将四级页表的某个入口映射到四级页表本身，实现了对所有当前和未来页表帧的映射。这种方法不需要额外的页表，并且可以简化页表的管理，但理解和实现相对复杂。

## 参考文献

https://ysos.gzti.me/labs/0x01/tasks/#%E5%8A%A0%E5%88%86%E9%A1%B9
