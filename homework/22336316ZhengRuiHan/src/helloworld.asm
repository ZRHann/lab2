section .data
    helloMessage db 'helloworld', 0xA  ; 'helloworld' 字符串和一个换行符
    helloLen equ $ - helloMessage      ; 字符串长度

section .text
    global _start

_start:
    ; 写消息到标准输出
    mov rax, 1              ; 'write' 系统调用的编号对于64位是1
    mov rdi, 1              ; 文件描述符 1 表示标准输出
    mov rsi, helloMessage   ; 消息的地址
    mov rdx, helloLen       ; 消息长度
    syscall                 ; 调用内核

    ; 退出程序
    mov rax, 60             ; 'exit' 系统调用的编号对于64位是60
    xor rdi, rdi            ; 退出码 0
    syscall                 ; 调用内核
