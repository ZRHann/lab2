// 引入 uart16550 包下的 SerialPort 结构体。这个结构体提供了操作串行端口的方法。
use super::uart16550::SerialPort;

// 定义 SERIAL_IO_PORT 常量，这是COM1端口的I/O地址。串行通信端口COM1通常映射到这个I/O地址。
const SERIAL_IO_PORT: u16 = 0x3F8; 

// 使用once_mutex宏定义一个全局的可变静态变量SERIAL，它将存储SerialPort类型的实例。这个宏确保了对SERIAL的访问是线程安全的。
once_mutex!(pub SERIAL: SerialPort);

// 定义init函数，用于初始化串行端口通信。
pub fn init() {
    // 使用SerialPort::new方法创建一个新的SerialPort实例，传入SERIAL_IO_PORT作为参数。
    // 然后调用init_SERIAL函数（由once_mutex!宏提供）来初始化SERIAL全局变量。
    init_SERIAL(SerialPort::new(SERIAL_IO_PORT));
    
    // 调用get_serial_for_sure函数（由guard_access_fn!宏提供）来安全地访问SERIAL变量，并调用其init方法进行初始化。
    // 这里的init方法配置了串行端口的各种参数，比如波特率。
    get_serial_for_sure().init();

    
    println!("{}", crate::get_ascii_header());
    // 打印一条消息到控制台，表示串行端口已经初始化完毕。
    println!("[+] Serial Initialized.");

}

// 使用guard_access_fn宏定义一个公开的函数get_serial，它提供了访问SERIAL变量的能力。
// 通过这个函数，其他模块可以安全地获取到SerialPort实例的引用。
guard_access_fn!(pub get_serial(SERIAL: SerialPort));
