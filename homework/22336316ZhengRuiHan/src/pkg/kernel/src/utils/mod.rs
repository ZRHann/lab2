#[macro_use]
mod macros;

pub mod logger;
pub use macros::*;

pub const fn get_ascii_header() -> &'static str {
    concat!(
        r"
  ___   ___   ____  ____     __  ____  __    __  
 |__ \ |__ \ |___ \|___ \   / / |___ \/_ |  / /  
    ) |   ) |  __) | __) | / /_   __) || | / /_  
   / /   / /  |__ < |__ < | '_ \ |__ < | || '_ \ 
  / /_  / /_  ___) |___) || (_) |___) || || (_) |
 |____||____||____/|____/  \___/|____/ |_| \___/ 
                                       v",
        env!("CARGO_PKG_VERSION")
    )
}
