#![no_std]
#![no_main]

#[macro_use]
extern crate log;

use core::arch::asm;
use ysos::print;
use ysos::println;
use ysos_kernel as ysos;

boot::entry_point!(kernel_main);

pub fn kernel_main(boot_info: &'static boot::BootInfo) -> ! {
    ysos::init(boot_info);
    print!("\x1B[2J\x1B[1;1H");
    println!("{}", ysos::get_ascii_header());
    loop {
        info!("Hello World from YatSenOS v2!");
        warn!("Hello World from YatSenOS v2!");
        error!("Hello World from YatSenOS v2!");
        for _ in 0..0x10000000 {
            unsafe {
                asm!("nop");
            }
        }
    }
}
