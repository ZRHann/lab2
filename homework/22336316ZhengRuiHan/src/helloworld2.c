#include <unistd.h> // 包含对于系统调用的定义，例如SYS_write的值

int main() {
    const char message[] = "Helloworld\n";
    asm volatile (
        "movq $1, %%rax\n" // 系统调用号为1 (SYS_write)
        "movq $1, %%rdi\n" // 文件描述符1 (标准输出)
        "movq %0, %%rsi\n" // 消息的地址
        "movq %1, %%rdx\n" // 消息的长度
        "syscall\n"        // 执行系统调用
        :
        : "r" (message), "r" ((long) sizeof(message) - 1)
        : "rax", "rdi", "rsi", "rdx"
    );
    return 0;
}
