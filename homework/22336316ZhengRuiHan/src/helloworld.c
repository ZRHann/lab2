#include <stdio.h>
#include <string.h>
int main() {
    unsigned char shellcode[] = 
    "\xb8\x01\x00\x00\x00"          // mov    $0x1,%eax
    "\xbf\x01\x00\x00\x00"          // mov    $0x1,%edi
    "\x48\xbe\xf5\xde\xff\xff"  // movabs $0x7fffffffde00,%rsi
    "\xff\x7f\x00\x00"
    "\xba\x0b\x00\x00\x00"          // mov    $0xb,%edx
    "\x0f\x05"                      // syscall
    "\xb8\x3c\x00\x00\x00"          // mov    $0x3c,%eax
    "\x48\x31\xff"                  // xor    %rdi,%rdi
    "\x0f\x05"                     // syscall
    "helloworld\n";
    void (*func)();
    func = (void (*)()) shellcode;
    (void)(*func)();
    return 0;
}